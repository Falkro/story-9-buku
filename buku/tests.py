from django.test import TestCase
from . import views

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve


# Create your tests here.

class BukuTest(TestCase):

    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_jeson_exist(self):
        response = Client().get('/jeson/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_using_jeson_func(self):
        found = resolve('/jeson/')
        self.assertEqual(found.func, views.jeson)
